from django.conf import settings

from live_studio.settings.hashes import HASHES

from .library import register

@register.simple_tag
def static(path):
    return settings.STATIC_MEDIA_URL % {
        'path': path,
        'hash': HASHES.get(path, '-'),
    }
