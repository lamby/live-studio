INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.admin',
    'django.contrib.messages',

    'debug_toolbar',
    'django_bcrypt',
    'django_extensions',
    'pedantic_http_methods',
    'south',

    'live_studio.auth',
    'live_studio.build',
    'live_studio.utils',
    'live_studio.debug',
    'live_studio.config',
    'live_studio.static',
    'live_studio.templatetags',
)
