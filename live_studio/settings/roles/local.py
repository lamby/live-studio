from os.path import dirname, join, abspath

DEBUG = True

LIVE_STUDIO_BASE = dirname(dirname(dirname(dirname(abspath(__file__)))))

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'live_studio.sqlite',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

BUILD_TEMP = join(LIVE_STUDIO_BASE, 'build-tmp')
BUILDS_ROOT = join(LIVE_STUDIO_BASE, 'builds')

ACTUALLY_BUILD_IMAGES = False

STATIC_MEDIA_URL = '/media/%(path)s'
