from __future__ import with_statement

import os
import sys
import time
import daemon
import shutil
import logging
import datetime
import lockfile
import subprocess

from django.conf import settings
from django.core.management.base import NoArgsCommand, make_option

from live_studio.build.models import Build
from live_studio.templatetags.text import command_line_options

POLL_EVERY = 2 # seconds

def call(logfile, args):
    logfile.write('$ %s\n' % command_line_options(args))
    logfile.flush()

    subprocess.check_call(args, stdout=logfile, stderr=logfile)

class Command(NoArgsCommand):
    option_list = NoArgsCommand.option_list + (
        make_option('--pidfile', dest='pidfile', help="Pidfile", default=None),
    )

    def handle_noargs(self, **options):
        if not options['pidfile']:
            self.run(options)
            return

        pidfile = lockfile.FileLock(options['pidfile'])

        with daemon.DaemonContext(pidfile=pidfile):
            self.run(options)

    def run(self, options):
        logging.basicConfig(level=logging.INFO)
        self.log = logging.getLogger('live-studio-runner')

        if int(options['verbosity']) > 1:
            self.log.setLevel(logging.DEBUG)

        while True:
            try:
                build = Build.objects.pop()

                def update(**kwargs):
                    self.log.debug('Updating #%d with %r', build.pk, kwargs)
                    Build.objects.filter(pk=build.pk).update(**kwargs)

                update(started=datetime.datetime.utcnow())

                shutil.rmtree(settings.BUILD_TEMP, ignore_errors=True)
                os.makedirs(settings.BUILD_TEMP)

                target_dir = os.path.join(settings.BUILDS_ROOT, build.ident)

                os.makedirs(target_dir)
                logfile = open(os.path.join(target_dir, 'log.txt'), 'a')

                self.log.info(
                    "Building #%d in %s", build.pk, settings.BUILD_TEMP,
                )

                try:
                    # Check disk space
                    st = os.statvfs(settings.BUILD_TEMP)
                    if st.f_bsize * st.f_bavail < settings.REQUIRE_FREE_SPACE_BYTES:
                        print >>logfile, "E: Not enough diskspace to build"
                        raise RuntimeError()

                    os.chdir(settings.BUILD_TEMP)

                    call(logfile, ('lb', 'config') + build.config.options())

                    if settings.ACTUALLY_BUILD_IMAGES:
                        call(logfile, ('lb', 'build'))
                    else:
                        open('binary.iso', 'w').write('iso here')

                    # Find file that was created
                    filename = None
                    for extension in ('iso', 'img'):
                        if not os.path.exists('binary.%s' % extension):
                            continue

                        filename = '%s.%s' % (build.ident, extension)
                        os.rename(
                            'binary.%s' % extension,
                            os.path.join(target_dir, filename),
                        )

                        break

                    assert filename, "Did not create any image"

                    update(
                        finished=datetime.datetime.utcnow(),
                        filename=filename,
                    )

                    self.log.info("#%d built successfully", build.pk)
                except:
                    update(finished=datetime.datetime.utcnow())
                    self.log.exception("#%d failed", build.pk)
                    continue
                finally:
                    os.chdir(settings.BUILD_TEMP)
                    call(logfile, ('lb', 'clean', '--purge'))
                    shutil.rmtree(settings.BUILD_TEMP)

                    self.log.info("Finished processing #%d", build.pk)

            except IndexError:
                self.log.debug('No items in queue, sleeping for %ds', POLL_EVERY)
                try:
                    time.sleep(POLL_EVERY)
                except KeyboardInterrupt:
                    sys.exit(1)
