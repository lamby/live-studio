from django.contrib import messages
from django.shortcuts import get_object_or_404, redirect
from django.views.decorators.http import require_POST
from django.contrib.auth.decorators import login_required

@require_POST
@login_required
def enqueue(request, config_id):
    config = get_object_or_404(request.user.configs, pk=config_id)

    config.builds.create()

    messages.info(request, "Enqueued.")

    return redirect(config)
