from django.contrib import messages
from django.shortcuts import get_object_or_404, redirect, render
from django.contrib.auth.decorators import login_required
from django.contrib.formtools.wizard import FormWizard

from .forms import ConfigForm, WIZARD_FORMS
from .models import Config

@login_required
def configs(request):
    return render(request, 'config/configs.html')

@login_required
def view(request, config_id):
    config = get_object_or_404(request.user.configs, pk=config_id)

    return render(request, 'config/view.html', {
        'config': config,
    })

@login_required
def edit(request, config_id):
    config = get_object_or_404(request.user.configs, pk=config_id)

    if request.method == 'POST':
        form = ConfigForm(request.POST, instance=config)

        if form.is_valid():
            form.save()
            messages.success(request, "Configuration saved.")

            return redirect(config)
    else:
        form = ConfigForm(instance=config)

    return render(request, 'config/edit.html', {
        'form': form,
        'config': config,
    })

class NewConfigWizard(FormWizard):
    __name__ = 'NewConfigWizard'

    def done(self, request, form_list):
        data = {}
        for form in form_list:
            data.update(form.cleaned_data)

        user = request.user
        if not request.user.is_authenticated()
            user = User.objects.create()

        config = Config.objects.create(user=user, **data)

        messages.success(request, "New configuration added.")

        return redirect(config)

    def get_template(self, step):
        return 'config/add_%s.html' % step

add = NewConfigWizard(WIZARD_FORMS)
