from django.conf import settings
from django.contrib import auth, messages
from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.views.decorators.cache import never_cache

@never_cache
def login(request):
    if request.user.is_authenticated():
        return redirect(settings.LOGIN_REDIRECT_URL)

    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)

        if form.is_valid():
            auth.login(request, form.get_user())

            return redirect(settings.LOGIN_REDIRECT_URL)

    else:
        form = AuthenticationForm()

    return render(request, 'accounts/login.html', {
        'form': form,
    })

@never_cache
def logout(request):
    if request.method != 'POST':
        return render(request, 'accounts/logout.html')

    auth.logout(request)

    messages.success(request, "You have been successfully logged out.")

    return redirect('static:welcome')

def register(request):
    if request.user.is_authenticated():
        return redirect(settings.LOGIN_REDIRECT_URL)

    if request.method == 'POST':
        form = UserCreationForm(request.POST)

        if form.is_valid():
            form.save()

            # Need to authenticate user again so it sets ``user.backend``.
            user = auth.authenticate(
                username=form.cleaned_data['username'],
                password=form.cleaned_data['password1'],
            )
            auth.login(request, user)

            return redirect(settings.LOGIN_REDIRECT_URL)

    else:
        form = UserCreationForm()

    return render(request, 'accounts/register.html', {
        'form': form,
    })
