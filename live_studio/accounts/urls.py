from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('live_studio.accounts.views',
    url(r'^login$', 'login',
        name='login'),
    url(r'^logout$', 'logout',
        name='logout'),
    url(r'^register$', 'register',
        name='register'),
)
