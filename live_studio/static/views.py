from django.conf import settings
from django.shortcuts import redirect, render

def welcome(request):
    if request.user.is_authenticated():
        return redirect(settings.LOGIN_REDIRECT_URL)

    return render(request, 'static/welcome.html')
