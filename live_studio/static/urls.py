from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('live_studio.static.views',
    url(r'^$', 'welcome',
        name='welcome'),
)
